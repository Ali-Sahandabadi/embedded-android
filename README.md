# Pong project - Embedded Android Course by BFH

## Introduction
This is a Pong game project to use hardware component of Firefly-BFH-Cape (Firefly-RK3399) board such as the display, buttons, LEDs, I2C(Ambient light sensor, Accelerometer sensor)

This project was developed by Android studio and was inspired by [Android Game Programming.](http://androidgameprogramming.com/programming-a-pong-game/)

* Android OS version: 4.4 KitKat
* Firefly-BFH-Cape (Firefly-RK3399)
* Accelerometer sensor: MPU-9250
* Ambient light sensor: MAX44009EDT+T 

## Role

* Play with potentiometer
* Play with Accelerometer Sensor(MPU9240)
* Multiplayer

> Note: If you want to play as Multiplayer, please be sure the Firefly-BFH-Cape is connected to the Internet. Otherwise, you will be notified by an android toast. 
The address of the server for the second player is [https://embedded-android.herokuapp.com](https://embedded-android.herokuapp.com).

## How to play 
[Pong game](https://en.wikipedia.org/wiki/Pong)

Button T1 -> single player mode with the potentiometer

Button T2 -> single player mode with the accelerometer

Button T3 -> Multiplayer player mode

Button T4 -> Start

You have just 4 lives, which are shown by LEDs and also by the display.

The speed of the game is increased by your score.


## installation

> ### Prerequisite
> You need to install android adb tool:
>```sh
> $ sudo apt install android-tools-adb
>```

First, be sure you are in the directory of BFH-Pong.apk, and your laptop is connected to Firefly-BFH-Cape. Then run below command to install the APK file:

```sh
$ adb install BFH-Pong.apk
```

## Permission

* INTERNET 
* ACCESS_NETWORK_STATE


## Dependencies
Include instructions on how to integrate the library into your projects. For instance install in your build.gradle:
```gradle
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'com.android.support:appcompat-v7:28.0.0'
    implementation 'com.android.support.constraint:constraint-layout:1.1.3'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.android.support.test:runner:1.0.2'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'
    implementation 'com.github.nkzawa:socket.io-client:0.3.0'
}
```
'com.github.nkzawa:socket.io-client:0.3.0' -> to use socket.io




## Deployment server side for Multiplayer


> ### Prerequisite
> Before everything, you should have installed [Git](https://www.atlassian.com/git/tutorials/install-git), [npm and node](https://www.npmjs.com/get-npm) on your local computer. Also, you need an account on [Heroku](http://heroku.com) and [Mlab](https://mlab.com).

### Configuration

Ensure you are connected to your Heroku account on your local machine.
```sh
$ heroku login
/// Please complete your authentication process
```

First, make an app on heroku. The name of the app should be a unique name.

Run below commands on your project directory. "embedded-android" was reserved here, you can choose another name for your app.

```sh
$ heroku create embedded-android
//Creating ⬢ embedded-android... done
//https://embedded-android.herokuapp.com.com/ | https://git.heroku.com/embedded-android.git
```

You can use the "git remote" command to confirm that a remote named Heroku has been set for your app:

```sh
$ git remote -v
heroku  https://git.heroku.com/embedded-android.git (fetch)
heroku  https://git.heroku.com/embedded-android.git (push)
```

But if you already made the app on heroku web, please follow ["For an existing Heroku app"](https://devcenter.heroku.com/articles/git#tracking-your-app-in-git) method.


After configuration, you don't need to repeat it again.


### Deployment on Heroku

Before you can deploy your app to Heroku, you need to initialize a local Git repository and commit your application code to it.

```sh
$ cd ..
$ git init
$ git add .
$ git commit -m "My first commit"
```
Deploy on heroku:

```sh
$ git push heroku master
```

Run your app URL, for example here "embedded-android" -> [https://embedded-android.herokuapp.com/](https://embedded-android.herokuapp.com/) 

## Code
Important function which you need to implmement on node.js server is :

```javascript
// This tag is for pong
io.on('connection', function(socket){
  socket.on('pong', function(msg){
    io.emit('pong', msg);
  });
});
````
This tag allows you to make a bridge through the node server. Then you can send your message with diffrent tags, and receive it by the same tag.   


## Appendix
[Socket.io for Android](https://socket.io/blog/native-socket-io-and-android/)

## Version control

| Version Number    | Purpose/Change    | Author 			| Date 		|
| ------------- 	|:-------------:	| :-----:			| ----:		|
| 0.1		      	| Initial draft 	| Ali Sahandabadi	| 05/04/2019 |
| 0.2		      	| Consultation draft 	| Ali Sahandabadi	| 08/04/2019 |

## Author and Contributors

![](https://gitlab.com/uploads/-/system/user/avatar/2428445/avatar.png?width=400) <br />

Ali Sahandabadi

<Ali.Sahandabadi@gmail.com> 



Please feel free to contact me.

License
----

* [MIT license](https://opensource.org/licenses/mit-license.php)

* Copyright @ [BFH](https://www.bfh.ch)

