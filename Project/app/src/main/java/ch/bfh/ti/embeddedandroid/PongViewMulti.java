package ch.bfh.ti.embeddedandroid;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.net.URISyntaxException;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;


public class PongViewMulti extends SurfaceView implements Runnable {

    // This is our thread
    Thread mGameThread = null;

    // This is new. We need a SurfaceHolder
    // When we use Paint and Canvas in a thread
    // We will see it in action in the draw method soon.
    SurfaceHolder mOurHolder;

    // A boolean which we will set and unset
    // when the game is running- or not
    volatile boolean mPlaying;

    // Game is mPaused at the start
    boolean mPaused = true;

    // A Canvas and a Paint object
    Canvas mCanvas;
    Paint mPaint;

    // This variable tracks the game frame rate
    long mFPS;

    // The size of the screen in pixels
    int mScreenX;
    int mScreenY;

    // The players mBat
    Bat mBat;
    BatUp mBatUp;

    // A mBall
    Ball mBall;

    // For sound FX
    SoundPool sp;
    int beep1ID = -1;
    int beep2ID = -1;
    int beep3ID = -1;
    int loseLifeID = -1;
    int explodeID = -1;

    // The mScore
    int mScore = 0;

    // Lives
    int mLives = 4;

    final char ON = '1';
    final char OFF = '0';


    /*
     * Define FireFly-BFH-CAPE LEDs and Buttons
     */
    final String LED_L1 = "124";
    final String LED_L2 = "125";
    final String LED_L3 = "126";
    final String LED_L4 = "127";
    // Button
    final String BUTTON_T1 = "120";
    final String BUTTON_T2 = "121";
    final String BUTTON_T3 = "122";
    final String BUTTON_T4 = "123";

    final String PRESSED = "0";

    //POTI 80 - 911
    static final String ADC_IN2 = "in_voltage2_raw"; // Channel-2 is for the potentiometer
    /*
     * Create the ADC object
     */
    final ADC adc = new ADC();
    String adcValue;
    Float intadcValue;

    //LED
    SysFs sysFs = new SysFs();


    boolean start = true;


    private boolean runningThread = true;
    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://embedded-android.herokuapp.com/");
        } catch (URISyntaxException e) {

            Log.d("faz", e.toString());
        }
    }


    /*
       When the we all new() on PongView
       This custom constructor runs
   */
    public PongViewMulti(Context context, int x, int y, Activity app) {
        /*
            The next line of code asks the
            SurfaceView class to set up our object.
        */
        super(context);

        // Set the screen width and height
        mScreenX = x;
        mScreenY = y;

        // Initialize mOurHolder and mPaint objects
        mOurHolder = getHolder();
        mPaint = new Paint();

        // A new mBat
        mBat = new Bat(mScreenX, mScreenY);
        mBatUp = new BatUp(mScreenX, 20);

        // Create a mBall
        mBall = new Ball(mScreenX, mScreenY);

        // Instantiate our sound pool dependent upon which version of Android
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();

            sp = new SoundPool.Builder()
                    .setMaxStreams(5)
                    .setAudioAttributes(audioAttributes)
                    .build();
        } else {
            sp = new SoundPool(5, AudioManager.STREAM_MUSIC, 0);
        }


        try{
            // Create objects of the 2 required classes
            AssetManager assetManager = context.getAssets();
            AssetFileDescriptor descriptor;

            // Load our fx in memory ready for use
            descriptor = assetManager.openFd("beep1.ogg");
            beep1ID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("beep2.ogg");
            beep2ID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("beep3.ogg");
            beep3ID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("loseLife.ogg");
            loseLifeID = sp.load(descriptor, 0);

            descriptor = assetManager.openFd("explode.ogg");
            explodeID = sp.load(descriptor, 0);

        }catch(IOException e){
            // Print an error message to the console
            Log.e("error", "failed to load sound files");
        }

        mSocket.on("pong", onNewMessage);
        mSocket.connect();




        setupAndRestart();

    }

    public void setupAndRestart(){

        // Put the mBall back to the start
        mBall.reset(mScreenX, mScreenY);



        //TODO
        new Thread(new Runnable() {
            public void run() {
                while (start) {

                    //
                    if (sysFs.read_value(BUTTON_T4).equals(PRESSED)){

                        mPaused = false;
                        Log.d("bb",sysFs.read_value(BUTTON_T4));
                        start = false;



                    }
                }

                //while (!Thread.currentThread().isInterrupted()) {
                while (runningThread){
                    adcValue = adc.read_adc(ADC_IN2);
                    Log.d("ttt", adcValue);
                    //77 - 915 -> 0 - 838

                    intadcValue = Float.valueOf(adcValue);
                    intadcValue -= 77;
                    intadcValue = (intadcValue / 838) * 647;
                    mBat.setMovementState(intadcValue);


                }
            }
        }).start();

        // if game over reset scores and mLives
        if(mLives == 0) {
            mScore = 0;
            mLives = 4;

            start = true;
        }

    }

    @Override
    public void run() {
        while (mPlaying) {

            // Capture the current time in milliseconds in startFrameTime
            long startFrameTime = System.currentTimeMillis();

            // Update the frame
            if(!mPaused){
                update();
            }

            // Draw the frame
            draw();

            /*
                Calculate the FPS this frame
                We can then use the result to
                time animations in the update methods.
            */
            long timeThisFrame = System.currentTimeMillis() - startFrameTime;
            if (timeThisFrame >= 1) {
                mFPS = 1000 / timeThisFrame;
            }

        }

    }

    // Everything that needs to be updated goes in here
    // Movement, collision detection etc.
    public void update() {

        // Move the mBat if required
        mBat.update(mFPS);
        mBatUp.update(mFPS);
        mBall.update(mFPS);


        // LED
        new Thread(new Runnable() {
            public void run() {

                if(mLives == 4){

                    sysFs.write_value(LED_L1, ON);
                    sysFs.write_value(LED_L2, ON);
                    sysFs.write_value(LED_L3, ON);
                    sysFs.write_value(LED_L4, ON);
                }
            }
        }).start();

        // Check for mBall colliding with mBat
        if(RectF.intersects(mBat.getRect(), mBall.getRect())) {
            mBall.setRandomXVelocity();
            mBall.reverseYVelocity();
            mBall.clearObstacleY(mBat.getRect().top - 2);

            mScore++;
            mBall.increaseVelocity();

            sp.play(beep1ID, 1, 1, 0, 0, 1);
        }

        if(RectF.intersects(mBatUp.getRect(), mBall.getRect())) {
            mBall.setRandomXVelocity();
            mBall.reverseYVelocity();
            mBall.clearObstacleYUp(mBatUp.getRect().bottom + 2);

            mScore++;
            mBall.increaseVelocity();

            sp.play(beep1ID, 1, 1, 0, 0, 1);
        }



        // Bounce the mBall back when it hits the bottom of screen
        if(mBall.getRect().bottom > mScreenY){
            mBall.reverseYVelocity();
            mBall.clearObstacleY(mScreenY - 2);

            // Lose a life
            mLives--;


            // LED
            new Thread(new Runnable() {
                public void run() {

                    switch(mLives) {

                        case 4 :
                            sysFs.write_value(LED_L1, ON);
                            sysFs.write_value(LED_L2, ON);
                            sysFs.write_value(LED_L3, ON);
                            sysFs.write_value(LED_L4, ON);

                            break;
                        case 3 :
                            // Statements
                            sysFs.write_value(LED_L1, ON);
                            sysFs.write_value(LED_L2, ON);
                            sysFs.write_value(LED_L3, ON);
                            sysFs.write_value(LED_L4, OFF);
                            break; // optional

                        case 2 :
                            // Statements
                            sysFs.write_value(LED_L1, ON);
                            sysFs.write_value(LED_L2, ON);
                            sysFs.write_value(LED_L3, OFF);
                            sysFs.write_value(LED_L4, OFF);
                            break; // optional

                        case 1 :
                            // Statements
                            sysFs.write_value(LED_L1, ON);
                            sysFs.write_value(LED_L2, OFF);
                            sysFs.write_value(LED_L3, OFF);
                            sysFs.write_value(LED_L4, OFF);
                            break; // optional
                        // You can have any number of case statements.
                        default : // Optional
                            // Statements
                            sysFs.write_value(LED_L1, OFF);
                            sysFs.write_value(LED_L2, OFF);
                            sysFs.write_value(LED_L3, OFF);
                            sysFs.write_value(LED_L4, OFF);
                    }



                }
            }).start();

            //TODO
            new Thread(new Runnable() {
                public void run() {
                    while (start) {

                        //
                        if (sysFs.read_value(BUTTON_T4).equals(PRESSED)){

                            mPaused = false;
                            Log.d("bb",sysFs.read_value(BUTTON_T4));
                            start = false;

                            //    }else{
                            //         start = true;
                        }
                    }

                }
            }).start();






            sp.play(loseLifeID, 1, 1, 0, 0, 1);

            if(mLives == 0){
                mPaused = true;
                start = false;
                setupAndRestart();
            }
        }

        // Bounce the mBall back when it hits the top of screen
        if(mBall.getRect().top < 0){
            mBall.reverseYVelocity();
            mBall.clearObstacleY(12);

            sp.play(beep2ID, 1, 1, 0, 0, 1);
        }

        // If the mBall hits left wall bounce
        if(mBall.getRect().left < 0){
            mBall.reverseXVelocity();
            mBall.clearObstacleX(2);

            sp.play(beep3ID, 1, 1, 0, 0, 1);
        }

        // If the mBall hits right wall bounce
        if(mBall.getRect().right > mScreenX){
            mBall.reverseXVelocity();
            mBall.clearObstacleX(mScreenX - 22);

            sp.play(beep3ID, 1, 1, 0, 0, 1);
        }

    }

    // Draw the newly updated scene
    public void draw() {

        // Make sure our drawing surface is valid or we crash
        if (mOurHolder.getSurface().isValid()) {
            // Lock the mCanvas ready to draw
            mCanvas = mOurHolder.lockCanvas();

            // Draw the background color
            mCanvas.drawColor(Color.argb(255, 255, 202, 89));

            // Choose the brush color for drawing
            mPaint.setColor(Color.argb(255, 104, 125, 143));

            // Draw the mBat
            mCanvas.drawRect(mBat.getRect(), mPaint);

            // Draw the mBatUp
            mCanvas.drawRect(mBatUp.getRect(), mPaint);

            // Draw the mBall
            mCanvas.drawRect(mBall.getRect(), mPaint);

            // Change the brush color for drawing
            mPaint.setColor(Color.argb(255, 249, 129, 0));

            // Choose the brush color for drawing
            mPaint.setColor(Color.argb(255, 255, 255, 255));

            // Draw the mScore
            mPaint.setTextSize(40);
            mCanvas.drawText("Score: " + mScore + "   Lives: " + mLives, 10, 50, mPaint);

            // Draw everything to the screen
            mOurHolder.unlockCanvasAndPost(mCanvas);
        }

    }

    // If the Activity is paused/stopped
    // shutdown our thread.
    public void pause() {
        mPlaying = false;
        try {
            mGameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }

    }

    // If the Activity starts/restarts
    // start our thread.
    public void resume() {
        mPlaying = true;
        mGameThread = new Thread(this);
        mGameThread.start();



    }
    //Destroy
    public void destroy(){
        // LED
        new Thread(new Runnable() {
            public void run() {



                sysFs.write_value(LED_L1, OFF);
                sysFs.write_value(LED_L2, OFF);
                sysFs.write_value(LED_L3, OFF);
                sysFs.write_value(LED_L4, OFF);

            }
        }).start();

        runningThread = false;


        mSocket.disconnect();
        mSocket.off("pong", onNewMessage);

    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            ((Activity) getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {

                        int data = (int) args[0];
                      //  int val = Integer.valueOf(data);
                       Log.d("fazdata", Integer.toString(data));
                        //Log.d("faz", data);


                        mBatUp.setMovementState(data);



                }
            });
        }
    };


}

