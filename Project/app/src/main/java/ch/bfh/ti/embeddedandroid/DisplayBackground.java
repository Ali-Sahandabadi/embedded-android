package ch.bfh.ti.embeddedandroid;

import android.util.Log;

import java.io.IOException;

/*
 * Java class for accessing the GPIOs by shell command via sysfs
 */

public class DisplayBackground
{
    public String gpio;
    public String value;


    /*
     *  Export a gpio
     */
    public boolean export(String gpio)
    {
        String[] shellCmd = {"/system/bin/sh","-c", String.format("echo %s > /sys/class/gpio/unexport", gpio)};
        try
        {
            Runtime.getRuntime().exec(shellCmd);
            Log.d("check1","True");
            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    /*
     * Unexport a gpio
     */
    public boolean unexport(String gpio)
    {
        String[] shellCmd = {"/system/bin/sh","-c", String.format("echo %s > /sys/class/pwm/pwmchip1/export", gpio)};
        try
        {
            Runtime.getRuntime().exec(shellCmd);
            Log.d("check2","True");
            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    /*
     * Set gpio direction to output
     */
    public boolean peroid(String gpio)
    {
        String[] shellCmd = {"/system/bin/sh","-c", String.format("echo %s > /sys/class/pwm/pwmchip1/pwm0/period", gpio)};
        try
        {
            Runtime.getRuntime().exec(shellCmd);
            Log.d("check3","True");

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    /*
     * Set gpio direction to input
     */
    public boolean duty_cycle(String gpio)
    {
        String[] shellCmd = {"/system/bin/sh","-c", String.format("echo %s > /sys/class/pwm/pwmchip1/pwm0/duty_cycle", gpio)};
        try
        {
            Runtime.getRuntime().exec(shellCmd);
            Log.d("check4","True");

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }

    /*
     * Write a value (0 or 1) to selected gpio number
     */
    public boolean enable(String gpio)
    {
        String[] shellCmd = {"/system/bin/sh","-c", String.format("echo %s > /sys/class/pwm/pwmchip1/pwm0/enable", gpio)};
        try
        {
            Runtime.getRuntime().exec(shellCmd);
            Log.d("check5","True");

            return true;
        }
        catch (IOException e)
        {
            return false;
        }
    }




}