package ch.bfh.ti.embeddedandroid;

import android.content.Intent;
import android.os.Bundle;

import android.app.Activity;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;

public class MainActivityAcc extends Activity {

    // pongView will be the view of the game
    // It will also hold the logic of the game
    // and respond to screen touches as well
    PongViewAcc pongView;


    private boolean runningThread = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get a Display object to access screen details
        Display display = getWindowManager().getDefaultDisplay();



        // Load the resolution into a Point object
        Point size = new Point();

        display.getSize(size);
        Log.d("display",size.toString());
        // 739 , 480
        // Initialize pongView and set it as the view
        pongView = new PongViewAcc(this, size.x, 450);
        setContentView(pongView);

    }


    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the pongView resume method to execute
        pongView.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the pongView pause method to execute
        pongView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pongView.destroy();
        runningThread = false;
       // this.finish();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MainActivityAcc.this, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }

}

