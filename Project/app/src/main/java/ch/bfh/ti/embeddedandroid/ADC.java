package ch.bfh.ti.embeddedandroid;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * Read a value from one of the eight ADC channels AIN0 ... AIN7 via sysfs
 */
public class ADC
 {  
  public String read_adc(String adc_channel)
   {
	Process p;
	String[] shellCmd = {"/system/bin/sh","-c", String.format("cat /sys/bus/iio/devices/iio:device0/" + adc_channel)};
	try
	 {
	  p = Runtime.getRuntime().exec(shellCmd);
	  BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
	  StringBuilder text = new StringBuilder();
	  String line;
	  while((line = reader.readLine()) != null)
	   {
		text.append(line);
	   }
	  return text.toString();
	 }
	catch (IOException e)
	 {
	  return "-1";
	 }
   }
 }
