package ch.bfh.ti.embeddedandroid;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class StartActivity extends AppCompatActivity {
    SysFs sysFs = new SysFs();
    SysFs sysFs1 = new SysFs();

    Thread backgroundThread, lightThread;
    // Button
    final String BUTTON_T1 = "120";
    final String BUTTON_T2 = "121";
    final String BUTTON_T3 = "122";
    final String BUTTON_T4 = "123";
    final String PRESSED = "0";


    /* MAX44009 Register pointers */
    private static final char MAX44009_CONFIG = 0x02;    /* Sensor Configuration Register */

    /* I2C Address of the MAX44009 device */
    private static final char MAX44009_I2C_ADDR = 0x4A;

    /* I2C object variable */
    I2C i2c;

    /* I2C device file name */
    private static final String MAX44009_FILE_NAME = "/dev/i2c-4";

    /* I2C Communication buffer and file handle */
    int[] i2cCommBuffer = new int[16];
    int fileHandle;

    /* Light and conversion variable */
    static int exponent;
    static int mantissa;
    static double luminance;
    // result for light d
    static int result;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        backgroundThread = new Thread(new Runnable() {
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {

                    //
                    if (sysFs.read_value(BUTTON_T1).equals(PRESSED)) {
                        Intent intent = new Intent(StartActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    }else if(sysFs.read_value(BUTTON_T2).equals(PRESSED)){
                        Intent intent = new Intent(StartActivity.this, MainActivityAcc.class);
                        startActivity(intent);
                        finish();
                        //backgroundThread.interrupt();
                    }else if(sysFs.read_value(BUTTON_T3).equals(PRESSED)){
                        Intent intent = new Intent(StartActivity.this, MainActivityMulti.class);
                        startActivity(intent);
                        finish();
                    }

                }
            }

        });

        backgroundThread.start();

        lightThread = new Thread(new Runnable() {
            public void run() {
                /* Instantiate the new i2c device */
                i2c = new I2C();

                /* Open the i2c device, get the file handle */
                fileHandle = i2c.open(MAX44009_FILE_NAME);

                /* Set the i2c slave address for all subsequent I2C device transfers */
                i2c.SetSlaveAddress(fileHandle, MAX44009_I2C_ADDR);

                /* Setup i2c buffer for the configuration register an write it to the MAX44009 device */
                /* Continuous mode, Integration time = 800 ms(0x40)	                                  */
                i2cCommBuffer[0] = MAX44009_CONFIG;
                i2cCommBuffer[1] = 0x40;
                i2c.write(fileHandle, i2cCommBuffer, 2);

                while (!Thread.currentThread().isInterrupted()) {
                    /* Setup the MAX44009 register to read the ambient light value */
                    i2cCommBuffer[0] = 0x03;
                    i2c.write(fileHandle, i2cCommBuffer, 1);
                    /* Read the current ambient light value from the MAX44009 device */
                    i2c.read(fileHandle, i2cCommBuffer, 2);

                    /* Convert the ambient light value to lux, have a look at the datasheet	*/
                    exponent = (i2cCommBuffer[0] & 0xF0) >> 4;
                    mantissa = ((i2cCommBuffer[0] & 0x0F) << 4) | (i2cCommBuffer[1] & 0x0F);
                    luminance = Math.pow(2, exponent) * mantissa * 0.045;

                    // 0 - 3000
                    // 0 - 25000
                    result =(int) (25000 - ((luminance / 3000) * 25000));

                    if (result > 18500){
                        result = 13000;
                    }
                    // PWM to control light of display background
                    sysFs1.write("/sys/class/pwm/pwmchip1/pwm0/duty_cycle",String.valueOf(result));

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        lightThread.start();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sysFs1.write("/sys/class/pwm/pwmchip1/pwm0/duty_cycle",String.valueOf(3000));
        /* Close the i2c file */
        i2c.close(fileHandle);
        backgroundThread.interrupt();
        lightThread.interrupt();
        moveTaskToBack(true);
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }


}
