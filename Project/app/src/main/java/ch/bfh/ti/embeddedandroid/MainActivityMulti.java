package ch.bfh.ti.embeddedandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class MainActivityMulti extends AppCompatActivity {

    // pongView will be the view of the game
    // It will also hold the logic of the game
    // and respond to screen touches as well
    PongViewMulti pongView;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket("https://embedded-android.herokuapp.com/");
            Log.d("faz", mSocket.toString());
        } catch (URISyntaxException e) {

            Log.d("faz", e.toString());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get a Display object to access screen details
        Display display = getWindowManager().getDefaultDisplay();



        // Load the resolution into a Point object
        Point size = new Point();

        display.getSize(size);
        Log.d("display",size.toString());
        // 739 , 480
        // Initialize pongView and set it as the view
        pongView = new PongViewMulti(this, size.x, 450,  MainActivityMulti.this);
        setContentView(pongView);



    }
    @Override
    protected void onStart() {
        super.onStart();
        if (isNetworkAvailable() == false ){
            Toast.makeText(this, "No internet, Please check your internet Connection!",
                    Toast.LENGTH_SHORT).show();
        }

    }

    // This method executes when the player starts the game
    @Override
    protected void onResume() {
        super.onResume();

        // Tell the pongView resume method to execute
        pongView.resume();
    }

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();

        // Tell the pongView pause method to execute
        pongView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        pongView.destroy();
        finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MainActivityMulti.this, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }




    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}